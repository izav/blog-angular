﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Blog.Controllers;
using Blog.Repositories;
using Blog.Repositories.Visitor;
using System.Data.Entity;
using System.Web.Security;
using Blog.Models.Visitor;
using Blog.Services;


namespace Blog
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory()); 
            Database.SetInitializer<DbContextEF>(new DbContextEFInitializer()); 
            Application["UserCount"] = 0;
        }



        public void AnonymousIdentification_Creating(Object sender, AnonymousIdentificationEventArgs e)
        {
            // Change the anonymous id
            e.AnonymousID = "A_" + DateTime.Now.Ticks;

            // Increment count of unique anonymous users
            Application["UserCount"] = Int32.Parse(Application["UserCount"].ToString()) + 1;
        }


        protected void Session_Start(Object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            if (currentContext != null)
            {
                if (!currentContext.Request.Browser.Crawler)
                {
                    WebVisitor currentVisitor = new WebVisitor(currentContext);
                    GeoLocationRestService service = new GeoLocationRestService();
                    var ip = currentVisitor.IpAddress == "::1" ? "" : currentVisitor.IpAddress;
                    currentVisitor.GeoLocation = service.GetGeoLocation(ip);
                    OnlineVisitorsContainer.Visitors[currentVisitor.SessionId] = currentVisitor;
                }
            }
        }


        protected void Session_End(Object sender, EventArgs e)
        {
            // Code that runs when a session ends.
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer
            // or SQLServer, the event is not raised.

            if (this.Session != null)
            {
                WebVisitor visitor;
                OnlineVisitorsContainer.Visitors.TryRemove(this.Session.SessionID, out visitor);
                IVisitorRepository<ArxiveVisitor, int> repository = new VisitorRepository(new DbContextEF());
                repository.SaveWebVisitor(visitor);

            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs eventArgs)
        {
            var session = HttpContext.Current.Session;
            if (session != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (OnlineVisitorsContainer.Visitors.ContainsKey(session.SessionID.ToString()))
                    OnlineVisitorsContainer.Visitors[session.SessionID].AuthUser = HttpContext.Current.User.Identity.Name;
            }
        }

    }
}
