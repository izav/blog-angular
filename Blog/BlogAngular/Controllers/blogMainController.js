﻿angular.module('blog').controller("blogMainCtrl", function ($scope, $uibModal, tagsInfo, blogResource) {
    $scope.tags = tagsInfo.data;
   
    $scope.tagSelected = function (tag) {
        $scope.currentTag = tag;
    };
   
    $scope.getTagTitleMessage = function (tagName) {
        return ('Get posts by tag ' + tagName);
    }


    $scope.openTooltip = function (postId, control, isLike) {
        isLike = isLike || true;
      
        blogResource.getNamesWhoLike(postId, isLike).then(function (responce) {
            var data = responce.data;
            $scope.userNamesWhoLike = "<div>" +
                       (data.count ? data.usernames.join(",<br>") : "No names") +
                       (data.count > data.numbertoshow ? "<br/>and " + (data.count - data.numbertoshow) +
                       " more ..." : "") + "</div>";
            control.popoverIsOpen = true;
        });
    }

   

});

angular.module('blog').controller("blogBlogPageCtrl", function ($scope,  pageInfo) {
   
    $scope.content = pageInfo.data.Content;
});



