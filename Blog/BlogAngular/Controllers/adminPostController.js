﻿angular.module('blog')
    .controller("adminPostCtrl", function ($scope, $state, $location, adminResource, blogSettings,
        postInfo, $uibModal) {

        $scope.post = postInfo.data.post;
        $scope.blogSettings = blogSettings;

        $scope.save = function (post) {
            adminResource.insertOrUpdatePost($scope.post).then(function (result) {
                if (result.data.post.Id > 0) {
                    $state.go('app.adminPostsByPage', { pageNo: 1 }, { reload: 'app.adminPostsByPage' });
                }
                else alert("not able to save");
            })
        };

        $scope.tinymceOptionsDescription = {
            charLimit: 3000,
            setup: function (editor) {
                editor.on('keyup', function (e) {
                    $scope.descriptionCounter = editor.getContent().length;
                });
                editor.on('loadcontent', function (e) {
                    $scope.descriptionCounter = editor.getContent().length;
                });
            },

            height: 200,
            theme: 'modern',
            plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,

            templates: [
              { title: 'Test template 1', content: 'Test 1' },
              { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tinymce.com/css/codepen.min.css'
            ]
        };

        $scope.tinymceOptionsBody = {
            onChange: function (e) {
               // $scope.bodyCounter = e.getContent().length;
            },
            height: 500,
            inline: false,
            plugins: 'advlist autolink link image lists charmap print preview',
            skin: 'lightgray',
            theme: 'modern'
        };
        
    });