﻿angular.module('blog')
    .controller("adminBlogPagesCtrl", function ($scope, pagesInfo) {

        $scope.orderBy = "Title";
        $scope.pages = pagesInfo.data;      
    })

angular.module('blog')
    .controller("adminBlogPageCtrl", function ($scope, $state, adminResource, pageInfo, blogSettings) {

        $scope.page = pageInfo.data;
        $scope.blogSettings = blogSettings;
        
        $scope.save = function () {
            adminResource.saveBlogPage($scope.page).then(function (result) {
                if (result.data > 0) {
                    $state.go('app.adminBlogPages');
                }
                else alert("not able to save");
            }, function () { alert("not able to save"); })
        };

        $scope.tinymceOptionsContent = {
            charLimit: $scope.blogSettings.MAXDESCRIPTION,
            setup: function (editor) {
                editor.on('keyup', function (e) {
                    $scope.contentCounter = editor.getContent().length;
                });
                editor.on('loadcontent', function (e) {
                    $scope.contentCounter = editor.getContent().length;
                });
            },

            height: 400,
            theme: 'modern',
            plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,

            templates: [
              { title: 'Test template 1', content: 'Test 1' },
              { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tinymce.com/css/codepen.min.css'
            ]
        };       
    });