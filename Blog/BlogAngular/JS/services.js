﻿//Settings ----------------------------
angular.module('blog')
    .constant('blogSettings', {
        MAXBODY: 3000,
        MAXDESCRIPTION: 100000,
    });

angular.module('blog')
    .value('userSettings', {

        pageSizeBlogPosts: 10,
        optionsBlogPosts: [5, 8, 10, 15, 20, 30, 40, 50, 100],

        pageSizeAdminPosts: 10,
        pageSizeAdminTags: 10,
        optionsAdminGrid: [5, 10, 15, 20, 40, 60, 80, 100]
    });



//Dialog ----------------------------
angular.module('blog').factory('dialogConfirm', function ($uibModal) {
    return function (message, title) {

        var modal = $uibModal.open({
            size: 'sm',
            template: '<div class="modal-header header-primary">\
                            <h4 class="modal-title" ng-bind="title"></h4>\
                        </div>\
                        <div class="modal-body" ng-bind="message"></div>\
                        <div class="modal-footer">\
                            <button class="btn btn-primary" ng-click="modal.close({result: true})">Yes</button>\
                            <button class="btn btn-primary" ng-click="modal.dismiss(\'cansel\')">No</button>\
                        </div>',
            controller: function ($scope, $uibModalInstance) {

                $scope.modal = $uibModalInstance;

                if (angular.isObject(message)) {
                    angular.extend($scope, message);
                } else {
                    $scope.message = message;
                    $scope.title = angular.isUndefined(title) ? '' : title;
                }
            }
        });

        return modal.result;
    }
})



.factory('dialogAlert', function ($uibModal) {

    return function (message, title) {

        var modal = $uibModal.open({
            size: 'sm',
            template: '<div class="modal-header">\
                        <h4 class="modal-title" ng-bind="title"></h4></div>\
                        <div class="modal-body" ng-bind="message"></div>\
                        <div class="modal-footer">\
                            <button class="btn btn-primary" ng-click="modal.close()">OK</button>\
                        </div>',
            controller: function ($scope, $uibModalInstance) {
                $scope.modal = $uibModalInstance;
                if (angular.isObject(message)) {
                    angular.extend($scope, message);
                } else {
                    $scope.message = message;
                    $scope.title = angular.isUndefined(title) ? '' : title;
                }
            }
        });

        return modal.result;
    }
});

    
//Blog---------------------------
angular.module('blog').factory('blogResource', ['$http', 'userSettings', function ($http, userSettings) {
  
    return {

        getBlogPage: function (description) {
            return $http.get("/Blog/GetBlogPage/" + description);
        },

        getPost: function (postId) {
            return $http.get("/Blog/GetPost/" + postId);
        },

        getPosts: function (pageNo) {
            return $http.get("/Blog/GetPosts", {
                params: {
                    id: pageNo || 1,
                    postsPerPage: userSettings.pageSizeBlogPosts
                }
            })
        },

        getPostsByTag: function (tagId, pageNo) {
            return $http.get("/Blog/GetPostsByTag/" + tagId, {
                params: {
                        page: pageNo || 1,
                        postsPerPage: userSettings.pageSizeBlogPosts
                }
            })
        },

        getExpandPost: function (postId) {
            return $http.get("/Blog/ExpandPost/" + postId);
        },
        
        getAddPostLike: function (postId, isLike) {
            return $http.get("/Blog/AddPostLike/" + postId, { params: { "like": isLike }});
        },
       
        getNamesWhoLike: function (postId, isLike) {
            return $http.get("/Blog/GetNamesWhoLike/" + postId, { params: { "isLike": isLike }});
        },

        getTags: function () {
            return $http.get("/Blog/GetTags");
        },
    }
    
}]);




//Admin-----------------
angular.module('blog').factory('adminResource', ['$http',  'userSettings',
       function ($http, userSettings) { return {

        getBlogPages: function () {

             return $http.get("/Admin/GetBlogPages");
        },

        getBlogPage: function (id) {
            return $http.get("/Admin/GetBlogPage/" + id);
        },

        saveBlogPage: function (blogPage) {
            return $http.post("/Admin/SaveBlogPage", { blogPage: blogPage });
        },
   
        getTags: function (pageNo) {
            return $http.get("/Admin/GetTags", {
                params: {
                    pageNo: pageNo || 1,
                    pageSize: userSettings.pageSizeAdminTags
                }
            })
        },

        insertOrUpdateTag: function (tag) {
            return $http.post("/Admin/InsertOrUpdateTag", { tag: tag });
        },

        getTag: function (id) {
            return $http.get("/Admin/GetTag/" + id);
        },

        deleteTag: function (id) {
            return $http.post("/Admin/DeleteTag/" + id);
        },

        getPosts: function (pageNo) {
            return $http.get("/Admin/GetPostsShortAjax", {
                params: {
                    pageNo: pageNo || 1,
                    pageSize: userSettings.pageSizeAdminPosts
                }
            })           
        },

        deletePost: function (id) {
            return $http.post("/Admin/DeletePost/" + id);
        },

        insertOrUpdatePost: function (post) {
            return $http.post("/Admin/InsertOrUpdatePostAjax", { post: post });
        },
      
        getPost: function (id) {
            return $http.get("/Admin/EditPostAjax/" + id);
        },

        getVisitedPages: function () {
            return $http.get("/Visitor/GetVisitedPages");
        },
        
        getVisitorsByIp: function () {
            return $http.get("/Visitor/GetAllVisitorsByIp");
        },
    }
}]);


//Disable link-----------------------------
angular.module('blog').directive('aDisabled', function () {
    return {
        compile: function (tElement, tAttrs, transclude) {
            //Disable ngClick
            tAttrs["ngClick"] = "!(" + tAttrs["aDisabled"] + ") && (" + tAttrs["ngClick"] + ")";

            //Toggle "disabled" to class when aDisabled becomes true
            return function (scope, iElement, iAttrs) {
                scope.$watch(iAttrs["aDisabled"], function (newValue) {
                    if (newValue !== undefined) {
                        iElement.toggleClass("disabled", newValue);
                    }
                });

                //Disable href on click
                iElement.on("click", function (e) {
                    if (scope.$eval(iAttrs["aDisabled"])) {
                        e.preventDefault();
                    }
                });
            };
        }
    };
});



//Visitor---------------------------
angular.module('blog').factory('visitorResource', ['$http',  function ($http) {

    return {
        getOnlineVisitorsMap: function () {
            return $http.get("/Visitor/GetOnlineVisitorsMap");
        },

        getOnlineVisitorsByCountriesMap: function () {
            return $http.get("/Visitor/GetOnlineVisitorsByCountriesMap");
        },

        getHistoryVisitorsMap: function (month, year) {
            return $http.get("/Visitor/GetHistoryVisitorsMap", {
                params: {
                    month: month | 0,
                    year: year | 0
                }
            });
        },

        getHistoryVisitorsMonthChart: function (month, year) {
                return $http.get("/Visitor/GetVisitorsForMonthChart", {
                    params: {
                        month: month | 0,
                        year: year | 0
                    }
                });
        },
    }

}]);

angular.module('blog').directive('highMap', function ($http) {

   
        return {
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            scope: {
                config: '='
            },
            link: function (scope, element, attrs) {
                var chart;
                var process = function () {
                    var defaultOptions = {
                        chart: { renderTo: element[0] },
                    };
                    var config = angular.extend(defaultOptions, scope.config);
                    chart = new Highcharts.Map(config);
                };
                process();
                scope.$watch("config.series", function (loading) {
                    process();
                });
                scope.$watch("config.loading", function (loading) {
                    if (!chart) {
                        return;
                    }
                    if (loading) {
                        chart.showLoading();
                    } else {
                        chart.hideLoading();
                    }
                });
            }
        };
   
});
