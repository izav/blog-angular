﻿<a href="http://www.flickr.com/photos/89565166@N02/8437316682/" title="ellis_cuttySark by Igor Sakkizov, on Flickr"><img src="http://farm9.staticflickr.com/8363/8437316682_3884c76c03.jpg" width="500" height="376" alt="ellis_cuttySark"></a>
<br/><br/>

В клиперах все было прекрасно - и функциональность, и облик, и имя. В истории мореплавания они остались самыми быстрыми, самыми красивыми и, увы, самыми недолговечными парусниками. 
<br/><br/>

Что же делало корабль клипером?
