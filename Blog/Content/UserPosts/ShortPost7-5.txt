﻿<a href="http://www.flickr.com/photos/igornasa/4336930146/" title="koloda_edited-1 by igornasa, on Flickr"><img src="http://farm5.static.flickr.com/4045/4336930146_25009ae078_m.jpg" width="240" height="190" alt="koloda_edited-1" /></a>
<br/><br/>

Из 200 млн. иммигрантов по всему миру на США приходится более 37 млн. (13% ее населения). Согласно опросу Гэллопа, США - самая желаемая страна для тех, кто подумывает об эмиграции. Из 700 млн. предполагающих эмигрировать 24% (165 млн) выбрали именно её.