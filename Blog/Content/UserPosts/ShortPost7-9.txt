﻿<img src="http://farm7.static.flickr.com/6114/6214303400_92d79253ac.jpg" width="500" height="334" alt="ElliLiberty">
<br/><br/>

<i>
Когда-то не было такой вещи, как нелегальная иммиграция в Америку.
<br/>
Если ты сумел добраться сюда, ты мог остаться здесь.
<br/>
(The Golden Door)
</i>
