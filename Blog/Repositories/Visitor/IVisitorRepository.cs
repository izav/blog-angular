﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Visitor;
using Blog.ViewModels;

namespace Blog.Repositories.Visitor
{
    public interface IVisitorRepository<T, TId> where T : class
    {
          void SaveWebVisitor(WebVisitor wv);
          Int64 SaveVisitorActionLog(VisitorActionLog log);

         IEnumerable<WebVisitor> GetAllVisitorsList();
         IEnumerable<StatisticsViewModel> GetVisitsForMonth(DateTime d1, DateTime d2);
         IEnumerable<StatisticsViewModel> GetVisitsForMonthByCountry(DateTime d1, DateTime d2);
         DateTime GetHistoryVisitorsStartDate();
         IEnumerable<VisitorActionsViewModel> GetVisitorActions();
         IEnumerable<WebVisitor> GetAllVisitorsByIp();

    }
}