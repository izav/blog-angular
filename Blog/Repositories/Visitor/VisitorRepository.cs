﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Visitor;
using Blog.Models.User;
using Blog.ViewModels;


namespace Blog.Repositories.Visitor
{
    public class VisitorRepository : IVisitorRepository<ArxiveVisitor, int>
    {
        private DbContextEF _context;
        public VisitorRepository(DbContextEF context)
        {
            _context = context;
        }

        public void SaveWebVisitor(WebVisitor wv)
        {
            if (wv != null)
            {
                var countryId = wv.GeoLocation.country_code;
                var country = _context.Countries.Find(countryId) ?? 
                       new Country() { Id = wv.GeoLocation.country_code, Name = wv.GeoLocation.country_name };

                string username = wv == null ? "" : wv.AuthUser;
                ApplicationUser user = username == "" ? null : _context.Users.Where(u => u.UserName == username).FirstOrDefault();
                WebVisitor visitor = wv;

                visitor.User = user;
                visitor.CountryId = countryId;
                visitor.Country = country;
                visitor.City = wv.GeoLocation.city;
                visitor.Region = wv.GeoLocation.region_code;
                visitor.Zip = wv.GeoLocation.zip_code;
                visitor.SessionEnded = DateTime.Now;
                _context.Visitors.Add(visitor);
                _context.SaveChanges();
            }
        }

        public Int64 SaveVisitorActionLog(VisitorActionLog log)
        {
            if (log.PostId != 0)
            {
                var user = _context.Posts
                    .Where(p => p.Id == log.PostId)
                    .FirstOrDefault();
             //   log.UserName = user == null ? "" : user.ApplicationUser.UserName;
            }


            var lastVisitorLog = _context.VisitorActionLogs
                          .Where(l => l.DateTime <= log.DateTime &&
                              l.SessionId == log.SessionId &&
                              l.Anonymous == log.Anonymous)
                          .OrderByDescending(t => t.DateTime).FirstOrDefault();
            if (lastVisitorLog != null)
            {
                
                var spentTime = log.DateTime.Subtract(lastVisitorLog.DateTime).Ticks;
                lastVisitorLog.SpentTimeMC = spentTime;
                _context.Entry(lastVisitorLog).Property(e => e.SpentTimeMC).IsModified = true;
            }

            _context.VisitorActionLogs.Add(log);
            _context.SaveChanges();

            var id = log.Id;
            return id;

        }


        //-----------------------------------------------------------
        public IEnumerable<StatisticsViewModel> GetVisitsForMonthByCountry(DateTime d1, DateTime d2)
        {

            IEnumerable<StatisticsViewModel> vm = _context.Visitors
                         .Where(v => v.SessionStarted >= d1 && v.SessionStarted <= d2)
                         .GroupBy(s => s.CountryId, s => s)
                         .Select(f => new StatisticsViewModel
                         {
                             Id2 = f.Key,
                             Title = f.FirstOrDefault().Country.Name,
                             Count1 = f.Count(), //visits
                             Count2 = f.Where(ff => ff.UserId != null) //users
                                                      .GroupBy(kk => kk.UserId, kk => kk.UserId)
                                                      .Select(pp => pp.Key).Count(),
                             Count3 = f.Where(ff => ff.UserId == null) // Anonymous
                                                      .GroupBy(kk => kk.IpAddress, kk => kk.IpAddress)
                                                      .Select(pp => pp.Key).Count(),
                         });
            return vm;

        }
        //Chart
        //--------------------------------------------------------------
        public IEnumerable<StatisticsViewModel> GetVisitsForMonth(DateTime d1, DateTime d2)
        {

            IEnumerable<StatisticsViewModel> vm = _context.Visitors
                         .Where(v => v.SessionStarted >= d1 && v.SessionStarted <= d2)
                         .GroupBy(s => s.SessionStarted.Day, s => s)
                         .Select(f => new StatisticsViewModel
                         {
                             Day = f.Key,
                             Count1 = f.Count(), //visits
                             Count2 = f.Where(ff => ff.UserId != null) //users
                                                      .GroupBy(kk => kk.UserId, kk => kk.UserId)
                                                      .Select(pp => pp.Key).Count(),
                             Count3 = f.Where(ff => ff.UserId == null) // Anonymous
                                                      .GroupBy(kk => kk.IpAddress, kk => kk.IpAddress)
                                                      .Select(pp => pp.Key).Count(),
                         });
            return vm;

        }
      
        //--------------------------------------------------------------
        public IEnumerable<WebVisitor> GetAllVisitorsList()
        {
            var av = _context.Visitors//.Include(h => h.User)
                .OrderBy(u => u.CountryId)
                .ThenBy(g => g.IpAddress)
                .ThenBy(g => g.User.UserName)
                .ThenBy(d => d.SessionStarted).ToList();
            return av;

        }
        //-----------------------------

        public DateTime GetHistoryVisitorsStartDate()
        {
            return DateTime.Now.AddYears(-1);
           // return _context.ArxiveVisitors.Count() == 0 ? DateTime.Now.AddMilliseconds(-3) : _context.ArxiveVisitors.Min(d => d.Date);
        }


        //------------------------
        public IEnumerable<VisitorActionsViewModel> GetVisitorActions()
        {
            var actions1 = _context.VisitorActionLogs
                    
                    .GroupBy(g => new { Key1 = g.Action, Key2 = g.PostId, Key3 =g.TagId})
                    .Select(f => new 
                    {

                        Action = f.Key.Key1,
                        Id1 = f.Key.Key2,
                        Id2 = f.Key.Key3,
                       
                        SpentTimeMC = f.Sum(ff => ff.SpentTimeMC),
                        Count1 = f.Count(),

                        Count2 = f.Where(u => u.VisitorUserName != "").GroupBy(ff => ff.VisitorUserName).Select(fff => fff.Key).Count(),

                        Count3 = f.GroupBy(a => a.Anonymous)
                                    .Select(s => s.Where(u => u.VisitorUserName != "").Count())
                                     .Where(x => x == 0).Count(),

                    });


            var actionsWithTag = actions1.GroupJoin(_context.Tags,
                               i1 => i1.Id2, i2 => i2.Id,
                               (i1, i2) => new 
                               {
                                   Action = i1.Action,
                                   Id1 = i1.Id1,
                                   Id2 = i1.Id2,
                                   SpentTimeMC = i1.SpentTimeMC,
                                   Count1 = i1.Count1,
                                   Count2 = i1.Count2,
                                   Count3 = i1.Count3,
                                   Title2 = i2.Any() ?  i2.FirstOrDefault().Name : "" ,
                               }

               );

            var actionsFull = actionsWithTag.GroupJoin(_context.Posts,
                                i1 => i1.Id1, i2 => i2.Id,
                                (i1, i2) => new VisitorActionsViewModel
                                {
                                    Action = i1.Action,
                                    Id1 = i1.Id1,
                                    Id2 = i1.Id2,
                                    SpentTimeMC = i1.SpentTimeMC,
                                    Count1 = i1.Count1,
                                    Count2 = i1.Count2,
                                    Count3 = i1.Count3,
                                    Title1 = i2.Any() ?  i2.FirstOrDefault().Title : "",
                                    Title2 = i1.Title2
                                }

                )
                .OrderBy(k => k.Action)
                .ThenByDescending(c => (c.SpentTimeMC) / (c.Count2 + c.Count3)).ThenBy(c => c.Action)
                .ThenBy(c => c.Title1)
                .ThenBy(c => c.Title2)
                .ToList();
            return actionsFull;

        }




        //-----------------------
        public IEnumerable<WebVisitor> GetAllVisitorsByIp()
        {
            var av = _context.Visitors
                .OrderBy(u => u.CountryId)
                .ThenBy(g => g.IpAddress)
                .ThenBy(g => g.User.UserName)
                .ThenBy(d => d.SessionStarted).ToList();
            return av;

        }

       //------------------------- 
    }
}