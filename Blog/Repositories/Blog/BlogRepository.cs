﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using Blog.Models.Blog;
using Blog.ViewModels;

namespace Blog.Repositories.Blog
{
    public class BlogRepository: IBlogRepository<Post, int>
    {
        private DbContextEF _context;
        public BlogRepository(DbContextEF context)
        {
            _context = context;
        }

        public BlogPage GetBlogPage(string description) {
            return _context.BlogPages
                           .Where(k => k.Description == description)
                           .FirstOrDefault();
        }


        public PostsViewModel GetPosts(int pageNo, int pageSize)
        {
            var vm = new PostsViewModel();
            var posts =  _context.Posts
                .Where(p => p.Published);

            var postsCount = posts.Count();

              vm.Posts =  posts.OrderByDescending(p => p.PostedOn)
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)                  
                    .Include(p => p.Tags)
                    .ToList();

              vm.PagesCount = (int)Math.Ceiling((double)postsCount / pageSize); 
            return vm;
        }

        public PostsViewModel GetPostsByTag(int id, int pageNo, int pageSize)
        {
          
            var vm = new PostsViewModel();
            var posts = _context.Tags.Where(t => t.Id == id)
                    .Include(t => t.Posts)
                    .SelectMany(k => k.Posts, (tt, kk)  => kk);
             var postsCount = posts.Count();
             vm.Posts=  posts.OrderByDescending(p => p.PostedOn)
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)                  
                    .Include(p => p.Tags)
                    .ToList();
             vm.PagesCount = (int)Math.Ceiling((double)postsCount / pageSize);
             return vm;
        }


        public Post ExpandPost (int id)
        {
            return _context.Posts.Find(id);
        }


        public Post GetPost(int id)
        {
            var post = _context.Posts
                    .Where(p => p.Id == id)
                    .Include(p => p.Tags)
                    .Include(p => p.Comments)
                    .FirstOrDefault();

            IEnumerable<int> ids = _context.Posts
                             .OrderByDescending(d => d.PostedOn)
                             .Select(k => k.Id)
                             .AsEnumerable()
                             .ToList();
            //paging
           // post.NextPostId = ids.SkipWhile(i => i != id).Skip(1).Select(i => i).FirstOrDefault();
            // need page number for for angular paging
           var nextPostInfo = ids.Select((k, index) => new {Id= k, Index = index })
                                 .SkipWhile( (i, index) => (i.Id != id))            
                                 .Skip(1).FirstOrDefault();
            post.PageNumber = nextPostInfo.Index;
            post.NextPostId = nextPostInfo.Id;
            post.PrevPostId = ids.TakeWhile(i => i != id).Select(i => i).LastOrDefault();
            post.LastPostId = ids.LastOrDefault();
            post.FirstPostId = ids.FirstOrDefault();
            post.PostsCount = ids.Count();
            return post;
                   
        }

        // get tags assigned for posts
        public IEnumerable<Tag> GetTags() {
            return _context.Tags
                   .Include(t => t.Posts)
                   .Where(t => t.Posts.Count() > 0) 
                   .Select(t => new  { Id = t.Id, Name = t.Name })
                   .OrderBy(n => n.Name)
                   .AsEnumerable()
                   .Select(t => new Tag() { Id = t.Id, Name = t.Name }).ToList();
        }

        //Comments
        public Comment AddComment(Comment comment)
        {
            //find last comments
            var comments = _context.Posts
                            .Where(p => comment.PostId == p.Id)
                            .Include(p => p.Comments)
                            .SelectMany(p => p.Comments)
                            .Select(k => new { Id = k.Id, ParentCommentId = k.ParentCommentId })
                            .ToList();
            if (comments.Count() == 0) comment.ParentCommentId = null;
            else 
            {
                var cmt =comments.Where(r => r.ParentCommentId == null).FirstOrDefault();
                int? parentCommentId = cmt.Id;
                while (cmt != null)
                {
                    parentCommentId = cmt.Id;
                    cmt = comments.Where(r => r.ParentCommentId == cmt.Id).FirstOrDefault();  
                }
                comment.ParentCommentId = parentCommentId;
            }

            var post = _context.Posts.Find(comment.PostId);
            post.CommentsCount += 1;
            _context.Comments.Add(comment);
            _context.Entry(post).State = EntityState.Modified;
            _context.SaveChanges();
            comment.Post = post;
            return comment;
        }
 
        //Likes----------------


        public IEnumerable<string> GetNamesWhoLike(int id,  bool like)
        {
            var names = _context.Posts
                .Where(p => p.Id == id)
                .Include(p => p.PostLikes)
                .SelectMany(l => l.PostLikes, (i1, i2) => i2)
                .Where(l => l.Like == like)
                .Select(l => l.User.UserName)
                .OrderBy(n => n).ToList();
            return names;
        }


        public int AddPostLike( int id, string username, bool like)
        { //0- not able to save, -1 - user liked, other - count
            Post post = _context.Posts.Find(id);
            if (post == null) return 0;
            
                
            PostLike postLike = _context.PostLikes
                           .Include("User")
                           .Where(x => x.User.UserName == username && x.PostId == id).FirstOrDefault();
            if (postLike != null) return -1;

            var user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();
            if (user == null) return 0;

            var postLikes = new PostLike { PostId = id, User = user, Like = like, DateTime = DateTime.Now };
            _context.PostLikes.Add(postLikes);

            if (like) post.PostLikesCount++;
            else post.PostDislikesCount++;
            _context.Entry(post).State = EntityState.Modified;

            _context.SaveChanges();
            return like ? post.PostLikesCount : post.PostDislikesCount;                  
            }

        public IEnumerable<Tag> GetTagsWithUsedCount()
        {

            return _context.Tags.Include(t => t.Posts)
                     .Select(t => new { Id = t.Id, Name = t.Name, TagUsedCount = t.Posts.Count() })
                     .OrderBy(t => t.Name)
                     .AsEnumerable()
                     .Select(t => new Tag { Id = t.Id, Name = t.Name, TagUsedCount = t.TagUsedCount })
                     .ToList();
        }
    }
}