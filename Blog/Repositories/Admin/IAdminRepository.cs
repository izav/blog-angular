﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using Blog.Models.Blog;
using Blog.ViewModels;

namespace Blog.Repositories.Admin
{
    public interface IAdminRepository<T, TId> where T : class
    {
        IEnumerable<BlogPageShortViewModel> GetBlogPagesShort();
        BlogPage GetBlogPage(int id);
        int UpdateBlogPage(BlogPage page);

        Post GetPost(int id);
        IEnumerable<PostShortViewModel> GetPostsShort(int pageNo, int pageSize);
        int GetPostsCount();
        int DeletePost(int id);
        Post InsertOrUpdatePost(Post post);

        IEnumerable<Tag> GetAllTags();
        IEnumerable<Tag> GetTags(int pageNo, int pageSize);
        int GetTagsCount();
        Tag InsertOrUpdateTag(Tag tag);
        int DeleteTag(int id);
        Tag GetTag(int id);
    }
}