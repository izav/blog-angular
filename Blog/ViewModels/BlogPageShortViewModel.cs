﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.ViewModels
{
    public class BlogPageShortViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}