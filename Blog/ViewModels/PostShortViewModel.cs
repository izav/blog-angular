﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Blog;

namespace Blog.ViewModels
{
    public class PostShortViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string PostedOn { get; set; }
        public bool Published { get; set; }
        public string Tags { get; set; }
        public int PrevPostId { get; set; }
        public int NextPostId { get; set; }
    }
}