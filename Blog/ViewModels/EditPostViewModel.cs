﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Blog;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels
{
    public class EditPostViewModel
    {
        public const int MAXDESCRIPTION = 3000;
        public const int MAXBODY = 100000;

       
        [Range(1, int.MaxValue, ErrorMessage = "At least one tag can be checked")]
        public int PostTagsCount { get; set; }

        [Range(1, MAXDESCRIPTION, ErrorMessage = "Description charachers count must be between 1 and 3000")]
        public int DescriptionCounter { get; set; }

        [Range(1, MAXBODY, ErrorMessage = "Body charachers count must be between 1 and 100000")]
        public int BodyCounter { get; set; }

        public Post Post { get; set; }
        public IEnumerable<PostTagViewModel> Tags { get; set; }
    }
}