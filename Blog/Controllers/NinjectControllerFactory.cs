﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Configuration;
using System.Web.Routing;
using System.Web.Mvc;
using Blog.Repositories.Blog;
using Blog.Repositories.Admin;
using Blog.Repositories.Visitor;
using Blog.Models.Blog;
using Blog.Models.Visitor;


// need to add to global.asax
// ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
namespace Blog.Controllers
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
                                    Type controllerType)
        {
            return controllerType == null
            ? null
            : (IController)kernel.Get(controllerType);
        }



        private void AddBindingsDB()
        {
            kernel.Bind<IBlogRepository<Post, int>>().To<BlogRepository>();
            kernel.Bind<IAdminRepository<Post, int>>().To<AdminRepository>();
            kernel.Bind<IVisitorRepository<ArxiveVisitor, int>>().To<VisitorRepository>();
        }
    }
}