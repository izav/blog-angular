﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Repositories;
using Blog.Repositories.Blog;
using Blog.Models.Blog;
using Blog.ViewModels;
using Blog.Filters;
using Blog.Models.Visitor;

namespace Blog.Controllers
{
    public class BlogController : Controller
    {
       public const int PostsPerPage = 10;
       private IBlogRepository<Post, int> _repo;

       public BlogController(IBlogRepository<Post, int> repo)
       {
           _repo = repo;
       }


       public ActionResult GetBlogPage(string id)
       {
           var description = id;
           var page = _repo.GetBlogPage(description);
           if (Request.IsAjaxRequest())
           {
               return Json(page , JsonRequestBehavior.AllowGet);
           }

           else
           {              
               return View(page);
           }

       }


        public ActionResult GetPosts(int id = 1, int postsPerPage = PostsPerPage)
        {
            var page = id;
            var vm = _repo.GetPosts(page, postsPerPage);
            
            vm.PageNumber = page;
            var IsAuthenticated = User.Identity.IsAuthenticated;
                          

            if (Request.IsAjaxRequest())
            {
                return Json(new
                {
                    CurrentPage = page,
                    PagesCount = vm.PagesCount,
                    PostsPerPage = PostsPerPage,
                    NextPage = page == vm.PagesCount ? page : page + 1,
                    PrevPage = page == 1 ? page : page - 1,
                    IsNext = page == vm.PagesCount,
                    IsPrev = page == 1,                    
                    IsAuthenticated = User.Identity.IsAuthenticated,

                    Posts = vm.Posts.Select(p => new
                    {
                        Id = p.Id,
                        Title = p.Title,
                        PostedOn = p.PostedOn.ToString("dd-MMM-yyyy"),
                        Description = p.Description,
                        CommentsCount = p.CommentsCount,
                        IsAuthenticated = IsAuthenticated,
                        PostDislikesCount = p.PostDislikesCount,
                        PostLikesCount = p.PostLikesCount,
                        Tags = p.Tags.Select(pp => new { Id = pp.Id, Name = pp.Name }).OrderBy(k => k.Name)
                    }).ToList()
                }, JsonRequestBehavior.AllowGet);
           
            }

            else
            {
                vm.Tags = _repo.GetTags();
                return View(vm);
            }
            
        }

        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, PostId = "id")]
        public ActionResult GetPost(int id)
        {
            var post = _repo.GetPost(id);
 
            if (post.CommentsCount > 0)
            {
                var comments = post.Comments.ToList();
                var commentsSorted = new List<Comment>();
                var comment = comments.Where(r => r.ParentCommentId == null).FirstOrDefault();

                while (comment != null) {
                    commentsSorted.Add(comment);
                    comment = comments.Where(r => r.ParentCommentId == comment.Id).FirstOrDefault();
                }
                post.Comments = commentsSorted;

            }
            
            if (Request.IsAjaxRequest())
            {
                return Json(
                            new
                             {
                                 post = new
                                       {
                                           Id = post.Id,
                                           Title = post.Title,
                                           PostedOn = post.PostedOn.ToString("dd-MMM-yyyy"),
                                           Description = post.Description,
                                           Body = post.Body,
                                           IsAuthenticated = User.Identity.IsAuthenticated,
                                           NextPostId = post.NextPostId,
                                           PrevPostId = post.PrevPostId,
                                           PageNumber = post.PageNumber,
                                           PostsCount = post.PostsCount,
                                           Tags = post.Tags.Select(pp => new { 
                                               Id = pp.Id, 
                                               Name = pp.Name 
                                           }),
                                           Comments = post.Comments.Select(c => new {
                                               CommentBody = c.CommentBody,
                                               PostedOn = c.PostedOn.ToString("dd-MMM-yyyy"),
                                               UserName = c.UserName 
                                           }),
                                           CommentsCount = post.CommentsCount,
                                           PostDislikesCount =post.PostDislikesCount,
                                           PostLikesCount = post.PostLikesCount,
                                       }
                             },
                               JsonRequestBehavior.AllowGet);
            }
            else return View(post);
        }

        [AllowAnonymous]
       // [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, TagId = "id")]
        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetPostsByTag(int id , int page = 1,  int postsPerPage =  PostsPerPage)
        {
            var vm = _repo.GetPostsByTag(id, page, postsPerPage);
            var IsAuthenticated = User.Identity.IsAuthenticated;
            return Json(
         new
           {
               CurrentPage = page,
               PagesCount = vm.PagesCount,
               PostsPerPage = PostsPerPage,
               NextPage = page == vm.PagesCount ? page : page +1,
               PrevPage = page == 1 ? page : page - 1,
               IsNext = page == vm.PagesCount,
               IsPrev = page == 1,
               TagId = id,
               IsAuthenticated = User.Identity.IsAuthenticated,

               Posts = vm.Posts.Select(p => new { 
                   Id = p.Id, 
                   Title = p.Title, 
                   PostedOn = p.PostedOn.ToString("dd-MMM-yyyy"),
                   Description = p.Description,
                   CommentsCount = p.CommentsCount,
                   IsAuthenticated = IsAuthenticated,
                   PostDislikesCount = p.PostDislikesCount,
                   PostLikesCount = p.PostLikesCount,
                   Tags= p.Tags.Select(pp => new {Id= pp.Id, Name = pp.Name}).OrderBy(k => k.Name)}).ToList()
           },
           JsonRequestBehavior.AllowGet);
        }



     //  [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult ExpandPost(int id)
       {
           var post = _repo.ExpandPost(id);
           return Json(
            new
            {
                  Body = post == null ? "Post body was not found" : post.Body,
            },
          JsonRequestBehavior.AllowGet);
       }



      // [ChildActionOnly]
       public ActionResult GetTags()
       {
           var tags = _repo.GetTags();
            if (Request.IsAjaxRequest())
            {
                return Json(
                            tags.OrderBy(t => t.Name).ToList(),
                                    JsonRequestBehavior.AllowGet);
           }
           else
           return PartialView("_Tags", tags);
       }

       [ChildActionOnly]
       public ActionResult GetTagsWithCount()
       {

           var tags = _repo.GetTagsWithUsedCount();

           return PartialView(tags);
       }



       [ValidateInput(false)]
       [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult AddComment(Comment comment)
       {
          var userName = User.Identity.Name;
          var IpAddress = HttpContext.Request.UserHostAddress;
          comment.UserName = (userName != null && userName != "") ? userName : "Anonymous IP: " + IpAddress;
          comment.PostedOn = DateTime.Now;

          var commentSaved = _repo.AddComment(comment);
          return Json(
          new
          {
              comment = new { 
                  Id = commentSaved.Id, 
                  UserName = commentSaved.UserName,
                  PostedOn = commentSaved.PostedOn.ToString("dd-MMM-yyyy"),
                  CommentBody = commentSaved.CommentBody,
                  CommetnsCount = commentSaved.Post.CommentsCount
              }
          },
          JsonRequestBehavior.AllowGet);
       }


     //  [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult AddPostLike(int id, bool like = true)
       {
           var userName = User.Identity.Name;
           int result = 0;
           if (userName != null && userName != "")
               result = _repo.AddPostLike(id, userName, like);
           return Json(
            new
            {
                result = result
            },
          JsonRequestBehavior.AllowGet);
       }


        [VisitorActionFilter(ActionType = (int)ActionType.SeeWhoLikes, PostId = "id")]
      // [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult GetNamesWhoLike(int id, bool like = true)
       {


           var result = _repo.GetNamesWhoLike(id,  like);
           return Json(
            new
            {
                likeordislike = like? "Like" :"Dislike",
                numbertoshow = 40,
                count = result.Count(), 
                usernames = result.ToArray()
            },
          JsonRequestBehavior.AllowGet);
       }

       

    }
}