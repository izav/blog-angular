[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Blog.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Blog.App_Start.NinjectWebCommon), "Stop")]

namespace Blog.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using System.Web.Http;
    using Ninject.Web.WebApi;
    using Blog.Repositories.Blog;
    using Blog.Repositories.Admin;
    using Blog.Repositories.Visitor;
    using Blog.Models.Blog;
    using Blog.Models.Visitor;



    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                //http://nodogmablog.bryanhogan.net/2016/04/web-api-2-and-ninject-how-to-make-them-work-together/
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);//iz
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IBlogRepository<Post, int>>().To<BlogRepository>();
            kernel.Bind<IAdminRepository<Post, int>>().To<AdminRepository>();
            kernel.Bind<IVisitorRepository<ArxiveVisitor, int>>().To<VisitorRepository>();
        
        }        
    }
}
