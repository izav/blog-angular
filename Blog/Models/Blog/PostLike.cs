﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Blog.Models.User;

namespace Blog.Models.Blog
{
    public class PostLike
    {
        public int Id { get; set; }


        public int PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public virtual ApplicationUser User { get; set; }
        public bool Like { get; set; }
        public DateTime DateTime { get; set; }
    }
}