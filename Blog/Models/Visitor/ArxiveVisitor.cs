﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Blog.Models.Visitor
{
    public class ArxiveVisitor
    {
        [Key]
        public int Id { get; set; }
        public int VisitorsCount { get; set; }
        public int UsersCount { get; set; }
        public DateTime Date { get; set; }
    }
}