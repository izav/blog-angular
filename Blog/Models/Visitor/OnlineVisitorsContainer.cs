﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using Blog.Models.Visitor;

namespace Blog.Models.Visitor
{
    public class OnlineVisitorsContainer
    {
        public static readonly ConcurrentDictionary<string, WebVisitor> Visitors = new ConcurrentDictionary<string, WebVisitor>();
    }
}