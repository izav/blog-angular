﻿using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;


namespace Blog.Filters
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            return HttpContext.Current.User.IsInRole("Admin");
        }
    }
}